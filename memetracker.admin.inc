<?php

/**
 * @file
 * FormAPI code for the Memetracker Admin pages.
 */


///*
// * Theme function for the memetracker edit page
// */
//function theme_memetracker_edit_form($memetracker) {
//  $link = l("Advanced", "memetracker/". $memetracker->mid ."/edit/advanced");
//  $output = $link;
//  $form = drupal_get_form('memetracker_edit_form', $memetracker);
//  $output .= drupal_render($form);
//  
//  return $output;
//}

/*
 * memetracker_edit_form
 */
function memetracker_edit_form($form_state, $memetracker) {

  // Prepare default values for form.
  $content_types = array();
  $content_sources = $memetracker->get_content_sources();
  foreach ($content_sources as $content_source) {
    $content_types[$content_source] = $content_source;
  }
  $basic_edit_link = l("Basic Edit Options", "memetracker/". $memetracker->mid 
  ."/edit");
  $advanced_link = l("Advanced Edit Options", "memetracker/". $memetracker->mid 
  ."/edit/advanced");
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Memetracker Name'),
    '#description' => t('Enter the name for this Memetracker'),
    '#default_value' => $memetracker->get_name(),
    '#required' => TRUE,
    '#prefix' => $basic_edit_link ." | ". $advanced_link,
    '#size' => 30,
  );
  $form['pickiness'] = array(
    '#type' => 'textfield',
    '#title' => t('Pickiness'),
    '#description' => t('Enter a number between 0 and 100 for how picky 
    memetracker will be when creating memes. A higher value means memetracker 
    will be more picky about what content makes it into a meme, a lower value,
     less picky, If memes seem to have unrelated content, try increasing the 
     pickiness value.'),
    '#default_value' => abs($memetracker->get_distance_threshold()*100 - 100),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['num_memes'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Memes'),
    '#description' => t('Enter the number of memes to be displayed'),
    '#default_value' => $memetracker->num_memes,
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['num_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Days of Content'),
    '#description' => t('Enter how many days of content to draw memes from'),
    '#default_value' => $memetracker->get_num_days(),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#default_value' => $content_types,
    '#options' => array(
      'content_source_drupal_feedapi' => t('RSS Feeds'),
      'content_source_drupal_node' => t('Internal Drupal Nodes'),
    ),
    '#description' => t('Choose the types of content that can be added to your
    Memetracker.'),
  );  
  $form['old_content_types'] = array(
    '#type' => 'hidden',
    '#value' => serialize($content_types),
  );
  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => $memetracker->get_mid(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );  
  return $form;
}

/*
 * Validate memetracker_edit_form
 */
function memetracker_edit_form_validate($form, &$form_state) {
  $pickiness = $form_state['values']['pickiness'];
  $num_memes = $form_state['values']['num_memes'];
  $num_days = $form_state['values']['num_days'];
  $name = $form_state['values']['name'];
  $content_types = $form_state['values']['content_types'];
  
  // Validate Memetracker Name
  if (empty($name)) {
    form_set_error('name', 'Please enter a name for the Memetracker');
  }
  // Validate distance threshold
  if ($pickiness < 0 || $pickiness > 100 || 
  !is_numeric($pickiness)) {
    form_set_error('pickiness', 'Enter a <em>number</em> between 
    0 and 100');
  }
  
  // Validate number of memes
  if (!is_numeric($num_memes) || $num_memes < 1) {
    form_set_error('num_memes', 'You must enter a positive number.');
  }
  
  // Validate number of days
  if (!is_numeric($num_days) || $num_days < 0.1) {
    form_set_error('num_memes', 'You must enter a positive number above 0.1.');
  }
  
  // Validate at least one content type is tracked
  if (!$content_types['content_source_drupal_feedapi'] 
    && !$content_types['content_source_drupal_node']) {
    form_set_error('content_types', 'You must select at least one type
    of content to follow.');
  }

}

/*
 * Submit memetracker_edit_form
 */
function memetracker_edit_form_submit($form, &$form_state) {
  $mid = $form_state['values']['mid'];
  $pickiness = $form_state['values']['pickiness'] / 100;
  $num_memes = $form_state['values']['num_memes'];
  $num_days = $form_state['values']['num_days'];
  $name = $form_state['values']['name'];  
  $old_content_types = $form_state['values']['old_content_types'];
  $content_types = $form_state['values']['content_types'];
  
  // Update values in memetracker table
  $result = db_query("UPDATE {memetracker} SET distance_threshold = %f,
  num_memes = %d, num_days = %f, name = '%s' 
  WHERE mid = %d", 
  abs($pickiness - 1), $num_memes, $num_days, check_plain($name), $mid);

  // Update Content Sources
  // check which cs are there, if there and not in new, disable
  // if not there and in new, insert
  foreach ($content_types as $content_type => $value) {
    if ($value) {
      // check if there's a row there already
      $cs = db_fetch_array(db_query("SELECT enabled 
      FROM {memetracker_content_source}
      WHERE cs_name = '%s' AND mid = %d", $content_type, $mid));
      
      // If content source row is already in database, continue.
      if ($cs['enabled']) {
        continue;
      }
      
      // If row for CS exists but is not enabled, we need to enable it.
      if (!empty($cs) && !$cs['enabled']) {
        db_query("UPDATE {memetracker_content_source} SET enabled = 1 
        WHERE cs_name = '%s' AND mid = %d", $content_type, $mid);
      }
      // If there's not a row yet, insert it.
      else{
        db_query("INSERT INTO {memetracker_content_source} VALUES (%d, '%s', 0, 
        '', 1)", $mid, $content_type);
      }
    }
    // For values equal to zero, disable the content source if it exists
    else {
      $is_there = db_result(db_query("SELECT mid FROM {memetracker_content_source}
      WHERE cs_name = '%s' AND mid = %d", $content_type, $mid));
      
      if ($is_there) {
        db_query("UPDATE {memetracker_content_source} SET enabled = 0
        WHERE cs_name = '%s' AND mid = %d", $content_type, $mid);
      }
    }
  }
  
  if ($result) {
    drupal_set_message(t("The memebrowser settings have been updated"));
  }
  else {
    form_set_error(Null, 'An error occurred updating your settings');
  }
  
  // Reset memes
  $memetracker = memetracker_load($mid, TRUE);
  $memetracker->get_memes(TRUE, FALSE);
  
  drupal_goto('memetracker/'. $mid);
}

function _set_hooks_info($hooks_info, $hooks_info_old, $info_array, 
$hook_string, $type, $description = "") {
  foreach ($info_array as $implementation) {
    if (empty($hooks_info[$hook_string]) || empty($hooks_info_old)){
      $hooks_info[$hook_string][$implementation['name']] = $implementation;
    }
    elseif (!in_array($implementation['name'], array_keys($hooks_info[$hook_string]))) {
      $hooks_info[$hook_string][$implementation['name']] = $implementation;
    }
    elseif (in_array($implementation['name'], array_keys($hooks_info_old[$hook_string]))) {
      $hooks_info[$hook_string][$implementation['name']]['value'] = 
      $hooks_info_old[$hook_string][$implementation['name']]['value'];
    }
  }
  
  // Set hook type to be used later when building the form. Type must equal
  // a valid FormAPI element name.
  $hooks_info[$hook_string]['type'] = $type;
  $hooks_info[$hook_string]['description'] = $description;
  if (isset($hooks_info_old[$hook_string]['value'])) {
    $hooks_info[$hook_string]['value'] = $hooks_info_old[$hook_string]['value'];
  }
  return $hooks_info;
}

function _memetracker_build_form_element($form, $hooks_info, $hook, $options) {
  switch($hooks_info[$hook]['type']) {
    case "radio":
      $form[$hook] = array(
        '#type' => 'fieldset',
        '#title' => t($hook .' Algorithms'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      
      $hook_names = array();
      $active = 0;
      $count = 0;
      foreach ($hooks_info[$hook] as $hookimpl) {
        if (is_array($hookimpl)) {
          $hook_names[] = $hookimpl['name'];
        }
      }
      $form[$hook][$hook] = array(
        '#type' => 'radios',
        '#title' => $hook,
        '#description' => $hooks_info[$hook]['description'],
        '#default_value' => $hooks_info[$hook]['value'],
        '#options' => $hook_names,
      );
      break;
    /*
    case "checkbox":
      $form[$hook] = array(
        '#type' => 'fieldset',
        '#title' => t($hook .' Algorithms'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      
      $options = array();
      foreach ($hooks_info[$hook] as $hookimpl) {
        if (is_array($hookimpl)) {
         // dpm($hookimpl);
          $options[$hookimpl['name']] = $hookimpl['description'];
        }
      }
      
      $form[$hook][$hook] = array(
        '#type' => 'checkboxes',
        '#title' => $hooks_info[$hook]['description'],
        '#description' => $hooks_info[$hook]['description'],
        '#default_value' => array('removeMultipleArticlesFromSameSource'), 
        '#options' => $options,
      );      
      break;
     */
    case "select":
      $form[$hook] = array(
        '#type' => 'fieldset',
        '#title' => t($hook .' Algorithms'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      foreach ($hooks_info[$hook] as $implementer) {
        if (is_array($implementer)) {
          $form[$hook][$implementer['name']] = array(
            '#type' => $hooks_info[$hook]['type'],
            '#title' => $implementer['name'],
            '#description' => $implementer['description'],
            '#options' => $options,
            '#default_value' => $implementer['value'],
          );
        }
      }
      
      break;
  }
  
  return $form;
}

function memetracker_edit_advanced_form($form_state, $memetracker) {
  // Get all settings from variable_get about previous set stuff
  $hooks_info_old = variable_get('memetracker_hooks', array());
  $hooks_info = array(); // Rebuild array to clear out old data upon changes.
  
  // Get info from all implementing hooks
  $headline = module_invoke_all('calculate_headline_info');
  $meme_interestingness = module_invoke_all('how_interesting_meme_info');
  $edit_meme_no_headline_info = module_invoke_all("edit_meme_no_head_line_info");

  // Build $hook_info array from info returned from implementing hooks.
  $hooks_info = _set_hooks_info($hooks_info, $hooks_info_old, $headline, 
  "Headline", "radio", "Choose an algorithm for calculating the headline of each meme.");
  $hooks_info = _set_hooks_info($hooks_info, $hooks_info_old, $meme_interestingness, 
  "Meme Interestingness", "select");
  $hooks_info = _set_hooks_info($hooks_info, $hooks_info_old, 
  $edit_meme_no_headline_info, "Edit meme before headline", "checkbox", 
  "Activate algorithms as needed to modify memes.");

  // Now the array is built, save so it can be accessed in the submmit function.
  variable_set("memetracker_hooks", $hooks_info);
  
  // Render info into forms
  $form = array();

  // Pull out names of every hook.
  $hooks = array_keys($hooks_info);
  
  // Loop through each hook creating a form for each module which implements 
  // a hook.
  $options = drupal_map_assoc(range(0, 10));
  foreach ($hooks as $hook) {
    $form += _memetracker_build_form_element($form, $hooks_info, $hook, $options);
  }
  
  $form['submit'] = array(
  '#type' => 'submit',
  '#value' => 'Submit',
  '#weight' => 10,
  );  
  return $form;
}

function memetracker_edit_advanced_form_validate($form, $form_state) {
  
}

function memetracker_edit_advanced_form_submit($form, $form_state) {
  $hooks_info = variable_get("memetracker_hooks", array());
  //dpm($form_state);
  // Pull off values and store.
  $hooks = array_keys($hooks_info);
  
  // big problem -- $form_state puts values in different places depending on whether there is one or two items in the form -- if 2 > x then it's in an array, otherwise it's on the first level.
  // Works for hooks that use a select form and saves set values.
  foreach ($hooks as $hook) {
    foreach ($hooks_info[$hook] as $implementer) {
      if (is_array($implementer) && isset($implementer['name'])) {
        $hooks_info[$hook][$implementer['name']]['value'] = 
        $form_state['values'][$implementer['name']];  
      }
        
    }
  }
  
  // Works for hooks that use radios and saves set values.
  foreach ($hooks as $hook) {
    // If value not set means that the hook isn't using radios or checkboxes.
    if(!is_null($form_state['values'][$hook]) 
    && !is_array($form_state['values'][$hook])) { 
      $hooks_info[$hook]['value'] = $form_state['values'][$hook];
    }  
  }
  
  // Works for hooks that use checkboxes and saves set values.
  foreach ($hooks as $hook) {
    // If value not set means that the hook isn't using radios or checkboxes.
    if(is_array($form_state['values'][$hook])) { 
      $hooks_info[$hook]['value'] = $form_state['values'][$hook];
    }  
  }
  
  
  // Save new settings
  variable_set("memetracker_hooks", $hooks_info);
  
  // TODO make default implments into an include file
  
  }
  
/*
 * memetracker_edit_feed_list_form
 */
function memetracker_edit_feed_list_form($form_state, $memetracker) {
  // Query for feeds being used already
  $data = db_result(db_query("SELECT data FROM {memetracker_content_source}
  WHERE mid = %d AND cs_name = 'content_source_drupal_feedapi'", 
  $memetracker->get_mid()));
  
  $data = unserialize($data);
  if (empty($data)) {
    $data = array();
  }
  
  // Query list of Feeds available through feedapi
  $results = db_query("SELECT f.nid, n.title FROM {feedapi} f JOIN {node} n
  WHERE f.nid = n.nid");
  
  // Loop through results and assemble an array to set as options in the form
  $options = array();
  while ($feed = db_fetch_array($results)) {
    $options[$feed['nid']] = $feed['title'];
  }
  $form['feeds'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Feeds'),
    '#default_value' => $data,
    '#options' => $options,
    '#description' => t('Select feeds to include in this memetracker'),
  );
  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => $memetracker->get_mid(),
  );
  $form['old_feed_list'] = array(
    '#type' => 'hidden',
    '#value' => serialize($data),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );  
  
  return $form;
}

/*
 * Validate memetracker_edit_form
 */
function memetracker_edit_feed_list_form_validate($form, &$form_state) {
  return;
}

/*
 * Submit memetracker_edit_form
 */
function memetracker_edit_feed_list_form_submit($form, &$form_state) {
// eventually I'll need to check if a source is being removed and for all sources
// that are removed, remove their accompanying content in memetracker_content

  $mid = $form_state['values']['mid'];
  $memetracker = memetracker_load($mid, TRUE);

  $old_feed_list = unserialize($form_state['values']['old_feed_list']);
  
  $checkboxes = $form_state['values']['feeds'];
  $feeds = array();
  // assemble array to insert into the database
  foreach ($checkboxes as $check) {
    // check not zero
    if ($check) {
      $feeds[] = $check;
    }
  }
  
  // Update feed list in database.
  $result = db_query("UPDATE {memetracker_content_source} SET data = '%s' WHERE 
  mid = %d", serialize($feeds), $mid);
  
  if ($result) {
    drupal_set_message(t("The memebrowser settings have been updated"));
  }
  else {
    form_set_error(Null, 'An error occurred updating your settings');
  }
  
  // Get array of feeds that are in the old list but not in the new list of 
  // feeds. Which, in other words, were removed from the list.
  $deleted_feeds = array_diff($old_feed_list, $feeds);
  if (!empty($deleted_feeds)) {
    $deleted_feeds_string = array_to_sql_string($deleted_feeds);

    // Delete content in memetracker_content that are part of deleted feeds
    db_query("DELETE m 
              FROM {memetracker_content} m JOIN {feedapi_node_item_feed} f 
              WHERE f.feed_item_nid = m.int_id 
              AND f.feed_nid IN ($deleted_feeds_string) 
              AND mid = %d", $memetracker->get_mid());
  }
  
  // Delete items from machinelearningapi_distance_cache
  
  // Get content for new feeds and reset memes
  $memetracker->check_new_content();
  
  $memetracker->get_memes(TRUE, FALSE);
  
  drupal_goto('memetracker/'. $mid);
}

/*
 * Refresh memetracker so check for new content and recalculate memes
 */
function memetracker_refresh($memetracker) {
  // Check for new content
  $memetracker->check_new_content();
  
  // Recalculate memes
  $memetracker->get_memes(TRUE, FALSE);
  
  // Return to memebrowsing page
  drupal_goto('memetracker/'. $memetracker->get_mid());
}

/*
 * Display stats about this memetracker's cron runs
 */
function memetracker_cron_stats($memetracker) {
  $table = "Statistics from memetracker's cron runs.";
  // Get data from memetracker_cron_stats.
  $result = db_query("SELECT timestamp, num_content, new_content,processing_time 
  FROM {memetracker_cron_stats} WHERE mid = %d ORDER BY timestamp DESC", 
  $memetracker->mid);
  // memory used, average interestingness of top memes, std dev in clusters?
  $header = array('Timestamp', 'Total Content',
   'New Content', 'Processing time');
  $rows = array();
  
  while ($data = db_fetch_array($result)) {
    $row = array(format_date($data['timestamp'], 'small'), $data['num_content'],
    $data['new_content'], $data['processing_time'] / 1000 .' seconds');
    
    $rows[] = $row;
  }

  // Theme.
  $table .= theme('table', $header, $rows);

  return $table;
}

/*
 * Form for creating new Memetrackers.
 */
function memetracker_new_form($form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Memetracker Name'),
    '#description' => t('Enter the name for this Memetracker'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['pickiness'] = array(
    '#type' => 'textfield',
    '#title' => t('Pickiness'),
    '#description' => t('Enter a number between 0 and 100 for how picky 
    memetracker will be when creating memes. A higher value means memetracker 
    will be more picky about what content makes it into a meme, a lower value,
     less picky, If memes seem to have unrelated content, try increasing the 
     pickiness value.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['num_memes'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Memes'),
    '#description' => t('Enter the number of memes to be displayed'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['num_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Days of Content'),
    '#description' => t('Enter how many days of content to draw memes from'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => array(
      'content_source_drupal_feedapi' => t('RSS Feeds'),
      'content_source_drupal_node' => t('Internal Drupal Nodes'),
    ),
    '#description' => t('Choose the types of content that can be added to your
    Memetracker.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );  
  return $form;
}

/*
 * Validate memetracker_edit_form
 */
function memetracker_new_form_validate($form, &$form_state) {
  $pickiness = $form_state['values']['pickiness'];
  $num_memes = $form_state['values']['num_memes'];
  $num_days = $form_state['values']['num_days'];
  $name = $form_state['values']['name'];
  $content_types = $form_state['values']['content_types'];
  
  // Validate Memetracker Name
  if (empty($name)) {
    form_set_error('name', 'Please enter a name for the Memetracker');
  }
  // Validate pickiness
  if ($pickiness < 0 || $pickiness > 100 || 
  !is_numeric($pickiness)) {
    form_set_error('pickiness', 'Enter a <em>number</em> between 
    0 and 100');
  }
  
  // Validate number of memes
  if (!is_numeric($num_memes) || $num_memes < 1) {
    form_set_error('num_memes', 'You must enter a positive number.');
  }
  
  // Validate number of days
  if (!is_numeric($num_days) || $num_days < 0.1) {
    form_set_error('num_days', 'You must enter a positive number above 0.1.');
  }
  
  // Validate at least one content type is tracked
  if (!$content_types['content_source_drupal_feedapi'] 
  && !$content_types['content_source_drupal_node']) {
    form_set_error('content_types', 'You must select at least one type
    of content to follow.');
  }
}

/*
 * Submit memetracker_edit_form
 */
function memetracker_new_form_submit($form, &$form_state) {
  $pickiness = $form_state['values']['pickiness'] / 100;
  $num_memes = $form_state['values']['num_memes'];
  $num_days = $form_state['values']['num_days'];
  $name = $form_state['values']['name'];  
  $content_types = $form_state['values']['content_types'];
  
  // Insert values in memetracker table.
  $result1 = db_query("INSERT INTO {memetracker} VALUES ('', 
  'simple_classifier1', '0', %f, %d, %f, '%s')",
  abs($pickiness - 1), $num_memes, $num_days, check_plain($name));
  
  // Retrive the mid of the new Memetracker.
  $mid = db_last_insert_id('memetracker', 'mid');
  
  // Insert content_source rows for the new Memetracker.
  foreach ($content_types as $content_type => $value) {
    // Insert only if checkbox selected.
    if ($value) {
      db_query("INSERT INTO {memetracker_content_source} 
      VALUES (%d, '%s', 0, '', 1)", $mid, $content_type);
    }
  }
  
  if ($result1) {
    drupal_set_message(t("Your new memetracker is created. Click the \"Edit Feed
    List\" tab to select feeds for your new memetracker to track."));
  }
  else {
    form_set_error(Null, 'An error occurred creating your memetracker');
  }

  // Go to new memebrowser page
  drupal_goto('memetracker/'. $mid);
}
