<?php

/**
 * @file meme.inc
 * Defines the meme object
 *
 */

require_once 'content.inc';


/**
 * class meme
 */
class meme {

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

  /**
   * @access private
   */
  public $headline;

  /**
   * @access private
   */
  public $discussion_array;

  /**
   * @access private
   */
  public $related_array;

  /**
   * @access private
   */
  public $interestingness;

  public function get_headline() {
    return $this->headline;
  }
  
  public function set_headline($headline) {
    $this->headline = $headline;
  }

  public function get_discussion_array() {
    return $this->discussion_array;
  }
  
  public function set_discussion_array($discussion_array) {
    $this->discussion_array = $discussion_array;
  }
  
  public function get_related_array() {
    return $this->related_array;
  }
  
  public function set_related_array($related_array) {
    $this->related_array = $related_array;
  }
  
  public function get_interestingness() {
    return $this->interestingness;
  }
  
  public function set_interestingness($interestingness) {
    $this->interestingness = $interestingness;  
  }
  
  public function get_sources_links() {
   $source_links == array();
  
   if($this->related_array) {
  
   foreach ($this->related_array as $content) {
     $source_links[] = $content->get_link();
   }
  
   }
   $source_links[] = $this->headline->get_link();
  
   return $source_links;
 }
  
  // accepts both an array or individual piece of content
  public function add_related_content($content) {
    if (is_null($content)) {
      return;
    }
    else {
      if (is_array($content)) {
        foreach ($content as $c) {
          if (!is_null($c)) {
            $this->related_array[] = $c;
          }
        }
      }
      else {
        $this->related_array[] = $content;
      }
    }
  }

  public function add_discussion_content($content) {
    $this->discussion_array[] = $content;
  }
} // end of meme
